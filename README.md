# Dylans ESPHome

## Install

## Run

```
esphome run <name>.yaml
```

## Renaming a device

```
esphome run new-device.yaml --device old-device
```

## How to make an air quality sensor

### Make connections

* PMS7003 (GND) to D1 Mini "GND" pin
* PMS7003 (VCC) to 5V
* PMS7003 pin 4 (RX) to D1 Mini "D6" GPIO12 pin
* PMS7003 pin 5 (TX) to D1 Mini "D4" GPIO2 pin

For BME680 temperature/gas sensor:
* BME680 VCC to 3V3
* BME680 GND to GND
* SCL to D1 GPIO5
* SCA to D2 GPIO4

## How to make garage opener

### Make connections

* D8 -> relay S (or IN)
* GND -> relay G (or DC-)
* 5V -> relay V (or DC+)
* Pair of wires from relay out

### Install the firmware

```
esphome run airquality_<name>.yaml
```

## Athom plugs

They use `name_add_mac_suffix` so you must look up their full name including
mac address in esphome and update using that:

```
esphome run office-plug.yaml --device office-plug-f1865a
```

## Debugging

### Remote logs

NOTE: Make sure you're on the IOT network

esphome logs airquality_<name>.yaml
